-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema dbbank
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dbbank
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dbbank` DEFAULT CHARACTER SET utf8 ;
USE `dbbank` ;

-- -----------------------------------------------------
-- Table `dbbank`.`Departamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`Departamento` (
  `Id_departamento` INT(11) NOT NULL,
  `Nombre_departamento` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id_departamento`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`Ciudad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`Ciudad` (
  `Id_ciudad` INT(11) NOT NULL,
  `Nombre_ciudad` VARCHAR(100) NOT NULL,
  `Id_departamento` INT(11) NOT NULL,
  PRIMARY KEY (`Id_ciudad`),
  INDEX `fk_Ciudad_departamento1_idx` (`Id_departamento` ASC),
  CONSTRAINT `fk_Ciudad_departamento1`
    FOREIGN KEY (`Id_departamento`)
    REFERENCES `dbbank`.`Departamento` (`Id_departamento`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`Ocupacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`Ocupacion` (
  `Id_ocupacion` INT(11) NOT NULL,
  `Descripcion` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`Id_ocupacion`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`TipoContrato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`TipoContrato` (
  `Id_tipo_contrato` INT(11) NOT NULL,
  `Descripcion` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`Id_tipo_contrato`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`TipoCredito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`TipoCredito` (
  `Id_tipo_credito` INT(11) NOT NULL,
  `Descripcion` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`Id_tipo_credito`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`Credito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`Credito` (
  `Id_credito` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre_empresa` VARCHAR(80) NULL DEFAULT NULL,
  `Fecha_ingreso` DATETIME NULL DEFAULT NULL,
  `Telefono_empresa` VARCHAR(50) NULL DEFAULT NULL,
  `Direccion_empresa` VARCHAR(150) NULL DEFAULT NULL,
  `Estado_credito` INT(11) NOT NULL,
  `Estado` BIT(1) NOT NULL,
  `Fecha_creacion` DATETIME NOT NULL,
  `Fecha_modificacion` DATETIME NOT NULL,
  `Id_tipo_contrato` INT(11) NULL DEFAULT NULL,
  `Id_ocupacion` INT(11) NOT NULL,
  `Id_tipo_credito` INT(11) NOT NULL,
  `Tipo_Credito` INT(11) NULL DEFAULT NULL,
  `Id_personas` INT(11) NOT NULL,
  PRIMARY KEY (`Id_credito`),
  INDEX `fk_Credito_TipoContrato1_idx` (`Id_tipo_contrato` ASC),
  INDEX `fk_Credito_Ocupacion1_idx` (`Id_ocupacion` ASC),
  INDEX `fk_Credito_TipoCredito1_idx` (`Id_tipo_credito` ASC),
  CONSTRAINT `fk_Credito_Ocupacion1`
    FOREIGN KEY (`Id_ocupacion`)
    REFERENCES `dbbank`.`Ocupacion` (`Id_ocupacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Credito_TipoContrato1`
    FOREIGN KEY (`Id_tipo_contrato`)
    REFERENCES `dbbank`.`TipoContrato` (`Id_tipo_contrato`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Credito_TipoCredito1`
    FOREIGN KEY (`Id_tipo_credito`)
    REFERENCES `dbbank`.`TipoCredito` (`Id_tipo_credito`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`Estadocivil`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`Estadocivil` (
  `Id_estado_civil` INT(11) NOT NULL,
  `Descripcion` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id_estado_civil`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`NivelAcademico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`NivelAcademico` (
  `Id_nivel_academico` INT(11) NOT NULL,
  `Descripcion` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id_nivel_academico`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`Personas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`Personas` (
  `Id_personas` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombres` VARCHAR(80) NOT NULL,
  `Apellidos` VARCHAR(80) NOT NULL,
  `Numero_documento` VARCHAR(50) NOT NULL,
  `Fceha_nacimiento` DATE NOT NULL,
  `Genero` CHAR(1) NOT NULL,
  `Email` VARCHAR(50) NOT NULL,
  `Celular` INT(11) NOT NULL,
  `Ingresos_mensuales` VARCHAR(50) NULL DEFAULT NULL,
  `Estrato` INT(11) NULL DEFAULT NULL,
  `Id_tipo_persona` INT(11) NOT NULL,
  `Fecha_creacion` DATETIME(1) NOT NULL,
  `Fecha_modificacion` DATETIME(1) NULL DEFAULT NULL,
  `Estado` BIT(1) NOT NULL,
  `Id_ciudad` INT(11) NOT NULL,
  `Id_estado_civil` INT(11) NOT NULL,
  `Id_nivel_academico` INT(11) NOT NULL,
  `Id_tipo_documento` INT(11) NOT NULL,
  `Id_Tipo_credito` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id_personas`))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`TipoDocumento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`TipoDocumento` (
  `Id_tipo_documento` INT(11) NOT NULL,
  `Descripcion` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id_tipo_documento`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `dbbank`.`Usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbbank`.`Usuarios` (
  `Id_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `Nombre_usuario` VARCHAR(45) NOT NULL,
  `Contraseña` VARCHAR(45) NOT NULL,
  `Estado` BIT(1) NOT NULL,
  `Fecha_creacion` DATETIME NOT NULL,
  `Personas` INT(11) NOT NULL,
  `Id_personas` INT(11) NOT NULL,
  `Id_credito` INT(11) NOT NULL,
  PRIMARY KEY (`Id_usuario`, `Id_personas`, `Id_credito`),
  INDEX `fk_Usuarios_Personas1_idx` (`Personas` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
